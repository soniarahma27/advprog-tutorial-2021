package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> chainSpells;

    public ChainSpell(ArrayList<Spell> chainSpells) {
        this.chainSpells = chainSpells;
    }

    @Override
    public void cast() {
        for (Spell spell : chainSpells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = chainSpells.size()-1 ; i >= 0; i--) {
            chainSpells.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
