##  Requirements

### ERD
![ERD](image/tutorial5.png)

- Mahasiswa
    - Mahasiswa dapat mendaftar ke satu mata kuliah
    - Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak bisa dihapus
    - Mahasiswa yang mendaftar ke suatu lowongan mata kuliah langsung diterima

- Mata Kuliah
    - Mata kuliah yang sudah ada asisten tidak dapat dihapus
    - Satu mata kuliah dapat menerima banyak mahasiswa
- Log
    - Mahasiswa dapat membuat log dan sistem akan menyimpannya.
    - Mahasiswa dapat memperbaharui maupun menghapus log yang telah dibuat
    - Mahasiswa dapat melihat laporan untuk bulan tertentu, yang berisi data bulan, jam kerja, dan pembayaran
    