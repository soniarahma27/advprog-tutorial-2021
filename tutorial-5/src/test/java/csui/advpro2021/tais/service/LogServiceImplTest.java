package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Autowired
    private MockMvc mvc;
    @Mock
    private LogRepository logRepository;
    @Mock
    private MataKuliahService mataKuliahService;
    @Mock
    private MahasiswaService mahasiswaService;
    @Mock
    private MahasiswaRepository mahasiswaRepository;
    @Mock
    private MataKuliahRepository mataKuliahRepository;
    @InjectMocks
    private LogServiceImpl logService;
    private Log log, log2;
    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        log = new Log("Log1", "01-01-2021 08:00", "01-01-2021 10:00");
        log2 = new Log("Log2", "02-01-2021 08:00", "02-01-2021 10:00");
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        mataKuliah = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
    }
    @Test
    public void testServiceDelete() {
        doNothing().when(logRepository).deleteById(anyInt());
        logService.deleteLogById(1);
    }
    @Test
    public void testServiceGetLog() {
        when(logRepository.findById(anyInt())).thenReturn(log);
        Log result = logService.logRepository.findById(log.getIdLog());
        assertEquals(result, log);
    }
    @Test
    public void testServiceUpdateLog() {
        when(logRepository.findById(anyInt())).thenReturn(log);
        when(logRepository.save(any(Log.class))).thenReturn(log2);
        Log logs = logService.updateLog(log.getIdLog(), log2);
        assertEquals(log2.getDescription(), log.getDescription());
        assertEquals(log2.getStartDate().getMinute(), log.getStartDate().getMinute());
        assertEquals(log2.getEndDate().getMinute(), log.getEndDate().getMinute());
    }
    @Test
    public void testServiceDaftarAsdos() {
        mataKuliah.setAsdos(new ArrayList<>());
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah(anyString())).thenReturn(mataKuliah);
        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(mahasiswa);
        when(mataKuliahRepository.save(any(MataKuliah.class))).thenReturn(mataKuliah);
        logService.daftarAsdos(mahasiswa.getNpm(), mataKuliah.getKodeMatkul());
        assertNotNull(mahasiswa.getMataKuliah());
        assertEquals(mahasiswa.getMataKuliah().getKodeMatkul(), mataKuliah.getKodeMatkul());
    }
    @Test
    public void testGetLogByMonth() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        logList.add(log2);
        mahasiswa.setLogMahasiswa(logList);
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        LogSummary result = logService.getLogByMonth(mahasiswa.getNpm(), "January");
        assertEquals(new LogSummary("January", 4, 1400), result);
    }
    @Test
    public void testGetAllSummary() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        mahasiswa.setLogMahasiswa(logList);
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        List<LogSummary> output = (List) logService.getAllLogSummary(mahasiswa.getNpm());
        assertEquals(output.size(), 1);
    }
}
