package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private LogService logService;
    private Log log;
    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        log = new Log("Log1", "01-01-2021 08:00", "01-01-2021 10:00");
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        mataKuliah = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
    }
    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
    @Test
    public void testControllerPostLog() throws Exception{

        when(logService.createLog(anyString(), any(Log.class))).thenReturn(log);

        mvc.perform(post("/log/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(log)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Log1"));
    }
    @Test
    public void testDeleteLog() throws Exception {
        mvc.perform(delete("/log/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }

    @Test
    public void testControllerUpdateLog() throws Exception{

        Log logs = new Log("Log2", "01-01-2021 08:00", "01-01-2021 10:00");

        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(logs);

        mvc.perform(put("/log/" + logs.getIdLog()).contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(logs)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Log2"));
    }

    @Test
    public void testControllerDaftarAsdos() throws Exception {
        when(logService.daftarAsdos(anyString(), anyString())).thenReturn(mahasiswa);

        mvc.perform(get("/log/register/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapToJson(mahasiswa)));

    }

    @Test
    public void testGetLogByMonth() throws Exception {
        LogSummary logSummary = new LogSummary("April", 2, 700);
        when(logService.getLogByMonth(anyString(), anyString())).thenReturn(logSummary);
        mvc.perform(get("/log/summary/" + mahasiswa.getNpm() + "/April")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(logSummary)));
    }
    @Test
    public void testGetAllLogSummary() throws Exception {
        List<LogSummary> logSummaries = new ArrayList<>();
        logSummaries.add(new LogSummary("January", 3, 1050));
        logSummaries.add(new LogSummary("Februari", 4, 1400));
        when(logService.getAllLogSummary(anyString())).thenReturn(logSummaries);
        mvc.perform(get("/log/summary/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(logSummaries)));
    }
    @Test
    public void testGetLog() throws Exception {
        Log logs = new Log("Log1", "01-01-2021 08:00", "01-01-2021 10:00");

        when(logService.getLog(anyInt())).thenReturn(logs);

        mvc.perform(get("/log/" + logs.getIdLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Log1"));
    }
}
