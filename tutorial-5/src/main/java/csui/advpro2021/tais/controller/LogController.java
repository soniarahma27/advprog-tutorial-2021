package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> createLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        log = logService.createLog(npm, log);
        return ResponseEntity.ok(log);
    }

    @GetMapping(path = "register/{npm}/{kodeMatkul}", produces = {"application/json"})
    public ResponseEntity<Mahasiswa> daftarAsdos(@PathVariable(value = "npm") String npm,
                                                   @PathVariable(value = "kodeMatkul") String kodeMatkul) {
        Mahasiswa mahasiswa = logService.daftarAsdos(npm, kodeMatkul);
        return ResponseEntity.ok(mahasiswa);
    }

    @GetMapping(path = "summary/{npm}/{month}", produces = {"application/json"})
    public ResponseEntity<LogSummary> getLogByMonth(@PathVariable(value = "npm") String npm,
                                                    @PathVariable(value = "month") String month) {
        LogSummary logSummary = logService.getLogByMonth(npm, month);
        return ResponseEntity.ok(logSummary);
    }

    @GetMapping(path = "summary/{npm}", produces = {"application/json"})
    public ResponseEntity<Iterable<LogSummary>> getAllLogSummary(@PathVariable(value = "npm") String npm) {
        Iterable<LogSummary> logSummaries = logService.getAllLogSummary(npm);
        return ResponseEntity.ok(logSummaries);
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> getLog(@PathVariable(value = "id") int id) {
        Log log = logService.getLog(id);
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public  ResponseEntity<Log> updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLogById(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLogById(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
