package csui.advpro2021.tais.model;

import lombok.Data;

@Data
public class LogSummary {
    private String month;
    private double jamKerja;
    private double pembayaran;

    public LogSummary(String month, double jamKerja, double pembayaran) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }
}
