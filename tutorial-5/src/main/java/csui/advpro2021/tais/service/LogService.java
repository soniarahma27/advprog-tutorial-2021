package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Log createLog(String npm, Log log);
    Mahasiswa daftarAsdos(String npm, String kodeMatkul);
    LogSummary getLogByMonth(String npm, String month);
    Iterable<LogSummary> getAllLogSummary(String npm);
    Log getLog(int id);
    Log updateLog(int id, Log log);
    void deleteLogById(int idLog);
}
