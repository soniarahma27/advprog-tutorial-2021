package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    MahasiswaRepository mahasiswaRepository;
    @Autowired
    MataKuliahRepository mataKuliahRepository;
    @Autowired
    LogRepository logRepository;
    @Autowired
    MahasiswaService mahasiswaService;
    @Autowired
    MataKuliahService mataKuliahService;

    @Override
    public Mahasiswa daftarAsdos(String npm, String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kodeMatkul);
        mahasiswa.setMataKuliah(mataKuliah);
        mataKuliah.getAsdos().add(mahasiswa);
        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);
        return mahasiswa;
    }

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        mahasiswa.getLogMahasiswa().add(log);
        mahasiswaRepository.save(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(int id) {
        return logRepository.findById(id);
    }

    @Override
    public void deleteLogById(int idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public Log updateLog(int id, Log log) {
        Log logs = logRepository.findById(id);
        logs.setDescription(log.getDescription());
        logs.setStartDate(log.getStartDate());
        logs.setEndDate(log.getEndDate());
        logRepository.save(logs);
        return logs;
    }

    @Override
    public LogSummary getLogByMonth(String npm, String month) {
        double jamKerja = 0;
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        Iterable<Log> logs = mahasiswa.getLogMahasiswa();
        for (Log log : logs) {
            if(log.getStartDate().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals(month)) {
                jamKerja += log.getDurasiKerja();
            }
        }
        return new LogSummary(month, jamKerja/60.0, jamKerja/60.0 * 350);
    }

    @Override
    public Iterable<LogSummary> getAllLogSummary(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        Iterable<Log> logs = mahasiswa.getLogMahasiswa();
        Calendar calendar = Calendar.getInstance();
        List<LogSummary> logSummaries = new ArrayList<>();
        double[] jamKerja = new double[12];
        for(Log log : logs) {
            int currentMonth = log.getStartDate().getMonthValue() - 1;
            jamKerja[currentMonth] +=log.getDurasiKerja();
        }
        for (int i = 0; i < 12; i++) {
            calendar.set(Calendar.MONTH, i + 1);
            String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
            if (jamKerja[i] == 0) continue;
            logSummaries.add(new LogSummary(month, jamKerja[i], jamKerja[i] * 350));
        }
        return logSummaries;
    }
}
