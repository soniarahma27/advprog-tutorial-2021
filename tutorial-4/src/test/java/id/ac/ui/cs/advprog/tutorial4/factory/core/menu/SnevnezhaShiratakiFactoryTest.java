package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;
    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }
    @Test
    public void testSnevnezhaShiratakiFactoryIsAMenuFactory() throws Exception {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory")));
    }
    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat =snevnezhaShiratakiFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiFactoryCreateFlavorMethod() {
        Flavor flavor = snevnezhaShiratakiFactory.createFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
        assertEquals(flavor.getDescription(), "Adding WanPlus Specialty MSG flavoring...");
    }
    @Test
    public void testSnevnezhaShiratakiFactoryCreateMeatmethod() {
        Meat meat = snevnezhaShiratakiFactory.createMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
        assertEquals(meat.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
    }
    @Test
    public void testSnevnezhaShiratakiFactoryCreateNoodleMethod() {
        Noodle noodle = snevnezhaShiratakiFactory.createNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki");
        assertEquals(noodle.getDescription(), "Adding Snevnezha Shirataki Noodles...");
    }
    @Test
    public void testSnevnezhaShiratakiFactoryCreateToppingMethod() {
        Topping topping = snevnezhaShiratakiFactory.createTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
        assertEquals(topping.getDescription(), "Adding Xinqin Flower Topping...");
    }
}
