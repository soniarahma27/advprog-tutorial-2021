package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    @Spy
    private MondoUdon mondoUdon = new MondoUdon("Mondo Udon");
    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }
    @Test
    public void testMondoUdonIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }
    @Test
    public void testMondoUdonIsAMenu() throws Exception {
        Collection<Type> classes = Arrays.asList(mondoUdonClass.getSuperclass());
        assertTrue(classes.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }
    @Test
    public void testMondoUdonAttribute() throws Exception {
        assertEquals(mondoUdon.getFlavor().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
        assertEquals(mondoUdon.getMeat().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        assertEquals(mondoUdon.getNoodle().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        assertEquals(mondoUdon.getTopping().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
        assertEquals(mondoUdon.getName(), "Mondo Udon");
    }
}
