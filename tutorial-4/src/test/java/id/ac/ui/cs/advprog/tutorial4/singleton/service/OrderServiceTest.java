package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceTest {
    private Class<?> orderServiceClass;
    @InjectMocks
    OrderService orderServiceImpl = new OrderServiceImpl();
    @Spy
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }
    @Test
    public void testOrderServiceGetFoodCorrectlyImplement() {
        orderServiceImpl.orderAFood("ramen");
        assertEquals("ramen", orderServiceImpl.getFood().toString());
    }
    @Test
    public void testOrderServiceGetDrinkCorrectlyImplement() {
        orderServiceImpl.orderADrink("kopi");
        assertEquals("kopi", orderServiceImpl.getDrink().toString());
    }
}
