package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MeatTest {
    private Class<?> meatClass;

    @BeforeEach
    public void setUp() throws Exception {
        meatClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat");
    }
    @Test
    public void testMeatIsAPublicInterface() throws Exception {
        int classModifier = meatClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }
    @Test
    public void testMeatHasGetDescriptionMethod() throws Exception {
        Method getDescription = meatClass.getDeclaredMethod("getDescription");
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
}
