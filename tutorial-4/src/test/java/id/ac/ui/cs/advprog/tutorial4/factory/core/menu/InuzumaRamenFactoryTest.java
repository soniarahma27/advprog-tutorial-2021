package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }
    @Test
    public void testInuzumaRamenFactoryIsAMenuFactory() throws Exception {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory")));
    }
    @Test
    public void testInuzumaRamenFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testInuzumaRamenFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }
    @Test
    public void testInuzumaRamenFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }
    @Test
    public void testInuzumaRamenFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testInuzumaRamenFactoryCreateFlavorMethod() throws Exception {
        Flavor flavor = inuzumaRamenFactory.createFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
        assertEquals(flavor.getDescription(), "Adding Liyuan Chili Powder...");
    }
    @Test
    public void testInuzumaRamenFactoryCreateMeatmethod() throws Exception {
        Meat meat = inuzumaRamenFactory.createMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
        assertEquals(meat.getDescription(), "Adding Tian Xu Pork Meat...");
    }
    @Test
    public void testInuzumaRamenFactoryCreateNoodleMethod() throws Exception {
        Noodle noodle = inuzumaRamenFactory.createNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen");
        assertEquals(noodle.getDescription(), "Adding Inuzuma Ramen Noodles...");
    }
    @Test
    public void testInuzumaRamenFactoryCreateToppingMethod() {
        Topping topping = inuzumaRamenFactory.createTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        assertEquals(topping.getDescription(), "Adding Guahuan Boiled Egg Topping");
    }
}
