package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UdonTest {
    private Class<?> udonClass;
    private Udon udon;

    @BeforeEach
    public void setUp() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        udon = new Udon();
    }
    @Test
    public void testUdonIsANoodle() throws Exception {
        Collection<Type> interfaces = Arrays.asList(udonClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }
    @Test
    public void testUdonOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = udonClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
    @Test
    public void testUdonGetDescriptionMethod() {
        String udonOutput = udon.getDescription();
        assertEquals(udonOutput.getClass().getName(), "java.lang.String");
        assertEquals(udonOutput, "Adding Mondo Udon Noodles...");
    }
}
