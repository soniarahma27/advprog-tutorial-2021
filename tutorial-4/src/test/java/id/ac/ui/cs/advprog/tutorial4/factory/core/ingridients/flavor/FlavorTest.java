package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlavorTest {
    private Class<?> flavorClass;

    @BeforeEach
    public void setUp() throws Exception {
        flavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor");
    }

    @Test
    public void testFlavorIsAPublicInterface() throws Exception {
        int classModifier = flavorClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }
    @Test
    public void tastFlavorHasGetDescriptionMethod() throws Exception {
        Method getDescripion = flavorClass.getDeclaredMethod("getDescription");
        assertEquals(0, getDescripion.getParameterCount());
        assertTrue(Modifier.isPublic(getDescripion.getModifiers()));
    }
}
