package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }
    @Test
    public void testLiyuanSobaFactoryIsAMenuFactory() throws Exception {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory")));
    }
    @Test
    public void testLiyuanSobaFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testLiyuanSobaFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = liyuanSobaFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }
    @Test
    public void testLiyuanSobaFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }
    @Test
    public void testLiyuanSobaFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = liyuanSobaFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testLiyuanSobaFactoryCreateFlavorMethod() {
        Flavor flavor = liyuanSobaFactory.createFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
        assertEquals(flavor.getDescription(), "Adding a dash of Sweet Soy Sauce...");
    }
    @Test
    public void testLiyuanSobaFactoryCreateMeatmethod() {
        Meat meat = liyuanSobaFactory.createMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
        assertEquals(meat.getDescription(), "Adding Maro Beef Meat...");
    }
    @Test
    public void testLiyuanSobaFactoryCreateNoodleMethod() {
        Noodle noodle = liyuanSobaFactory.createNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba");
        assertEquals(noodle.getDescription(), "Adding Liyuan Soba Noodles...");
    }
    @Test
    public void testLiyuanSobaFactoryCreateToppingMethod() {
        Topping topping = liyuanSobaFactory.createTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
        assertEquals(topping.getDescription(), "Adding Shiitake Mushroom Topping...");
    }
}
