package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuRepositoryTest {
    MenuRepository menuRepository;
    @BeforeEach
    public void setUp() throws Exception {
        menuRepository = new MenuRepository();
    }
    @Test
    public void testMenuRepositoryList(){
        assertTrue(menuRepository.getMenus() instanceof ArrayList);
    }
    @Test
    public void testAddMenuList(){
        Menu menu = new InuzumaRamen("Inuzuma Ramen");
        menuRepository.add(menu);
        List<Menu> lst = menuRepository.getMenus();
        assertTrue(lst.contains(menu));
        assertEquals(lst.size(),1);
    }
}
