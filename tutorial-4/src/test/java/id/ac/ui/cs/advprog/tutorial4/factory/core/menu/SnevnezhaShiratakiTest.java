package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    @Spy
    private SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("Snevnezha Shirataki");
    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
    }
    @Test
    public void testSnevnezhaShiratakiIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiIsAMenu() throws Exception {
        Collection<Type> classes = Arrays.asList(snevnezhaShiratakiClass.getSuperclass());
        assertTrue(classes.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }
    @Test
    public void testSnevnezhaShiratakiAttribute() throws Exception {
        assertEquals(snevnezhaShirataki.getFlavor().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
        assertEquals(snevnezhaShirataki.getMeat().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
        assertEquals(snevnezhaShirataki.getNoodle().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki");
        assertEquals(snevnezhaShirataki.getTopping().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
        assertEquals(snevnezhaShirataki.getName(), "Snevnezha Shirataki");
    }
}
