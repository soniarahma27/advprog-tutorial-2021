package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuFactoryTest {
    private Class<?> menuFactoryClass;
    @BeforeEach
    public void setUp() throws Exception {
        menuFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory");
    }
    @Test
    public void testMenuFactoryIsAPublicInterface() throws Exception {
        int classModifier = menuFactoryClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }
    @Test
    public void testToppingHasCreateFlavorMethod() throws Exception {
        Method createFlavor = menuFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testToppingHasCreateMeatMethod() throws Exception {
        Method createMeat = menuFactoryClass.getDeclaredMethod("createMeat");
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }
    @Test
    public void testToppingHasCreateNoodleMethod() throws Exception {
        Method createNoodle = menuFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }
    @Test
    public void testToppingHasCreateToppingMethod() throws Exception {
        Method createTopping = menuFactoryClass.getDeclaredMethod("createTopping");
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
}
