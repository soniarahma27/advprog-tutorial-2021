package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ToppingTest {
    private Class<?> toppingClass;
    @BeforeEach
    public void setUp() throws Exception {
        toppingClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping");
    }
    @Test
    public void testNoodleIsAPublicInterface() throws Exception {
        int classModifier = toppingClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }
    @Test
    public void testToppingHasDescriptionMethod() throws Exception {
        Method getDescription = toppingClass.getDeclaredMethod("getDescription");
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
}
