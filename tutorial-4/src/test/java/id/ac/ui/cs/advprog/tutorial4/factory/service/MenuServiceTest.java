package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @InjectMocks
    MenuService menuService = new MenuServiceImpl();

    @Mock
    MenuRepository menuRepository;

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }
    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        assertTrue(Modifier.isPublic(getMenus.getModifiers()));
        ParameterizedType parameterizedType = (ParameterizedType) getMenus.getGenericReturnType();
        assertTrue(Arrays.asList(parameterizedType.getActualTypeArguments()).contains(Menu.class));
        assertEquals(List.class,parameterizedType.getRawType());
    }
}
