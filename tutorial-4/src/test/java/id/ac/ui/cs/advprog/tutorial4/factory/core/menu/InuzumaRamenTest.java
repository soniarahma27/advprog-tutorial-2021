package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    @Spy
    private InuzumaRamen inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");
    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }
    @Test
    public void testInuzumaRamenIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }
    @Test
    public void testInuzumaRamenIsAMenu() throws Exception {
        Collection<Type> classes = Arrays.asList(inuzumaRamenClass.getSuperclass());
        assertTrue(classes.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }
    @Test
    public void testInuzumaRamenAttribute() throws Exception {
        assertEquals(inuzumaRamen.getFlavor().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
        assertEquals(inuzumaRamen.getMeat().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
        assertEquals(inuzumaRamen.getNoodle().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen");
        assertEquals(inuzumaRamen.getTopping().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        assertEquals(inuzumaRamen.getName(), "Inuzuma Ramen");
    }
}
