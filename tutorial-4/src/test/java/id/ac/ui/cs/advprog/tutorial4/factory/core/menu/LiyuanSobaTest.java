package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    @Spy
    private LiyuanSoba liyuanSoba = new LiyuanSoba("Liyuan Soba");
    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
    }
    @Test
    public void testLiyuanSobaIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }
    @Test
    public void testLiyuanSobaIsAMenu() throws Exception {
        Collection<Type> classes = Arrays.asList(liyuanSobaClass.getSuperclass());
        assertTrue(classes.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }
    @Test
    public void testLiyuanSobaAttribute() throws Exception {
        assertEquals(liyuanSoba.getFlavor().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
        assertEquals(liyuanSoba.getMeat().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
        assertEquals(liyuanSoba.getNoodle().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba");
        assertEquals(liyuanSoba.getTopping().getClass().getTypeName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
        assertEquals(liyuanSoba.getName(), "Liyuan Soba");
    }
}
