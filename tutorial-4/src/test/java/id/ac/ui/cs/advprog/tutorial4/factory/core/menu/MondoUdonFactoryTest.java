package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    private MondoUdonFactory mondoUdonFactory;
    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }
    @Test
    public void testMondoUdonFactoryIsAMenuFactory() throws Exception {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory")));
    }
    @Test
    public void testMondoUdonFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonFactoryClass.getDeclaredMethod("createFlavor");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testMondoUdonFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonFactoryClass.getDeclaredMethod("createMeat");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }
    @Test
    public void testMondoUdonFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonFactoryClass.getDeclaredMethod("createNoodle");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }
    @Test
    public void testMondoUdonFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonFactoryClass.getDeclaredMethod("createTopping");
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testMondoUdonFactoryCreateFlavorMethod() {
        Flavor flavor = mondoUdonFactory.createFlavor();
        assertEquals(flavor.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
        assertEquals(flavor.getDescription(), "Adding a pinch of salt...");
    }
    @Test
    public void testMondoUdonFactoryCreateMeatmethod() {
        Meat meat = mondoUdonFactory.createMeat();
        assertEquals(meat.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        assertEquals(meat.getDescription(), "Adding Wintervale Chicken Meat...");
    }
    @Test
    public void testMondoUdonFactoryCreateNoodleMethod() {
        Noodle noodle = mondoUdonFactory.createNoodle();
        assertEquals(noodle.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        assertEquals(noodle.getDescription(), "Adding Mondo Udon Noodles...");
    }
    @Test
    public void testMondoUdonFactoryCreateToppingMethod() {
        Topping topping = mondoUdonFactory.createTopping();
        assertEquals(topping.getClass().getName(),
                "id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
        assertEquals(topping.getDescription(), "Adding Shredded Cheese Topping...");
    }
}
