package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PorkTest {
    private Class<?> porkClass;
    private Pork pork;

    @BeforeEach
    public void setUp() throws Exception {
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
        pork = new Pork();
    }
    @Test
    public void testPorkIsAMeat() throws Exception {
        Collection<Type> interfaces = Arrays.asList(porkClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }
    @Test
    public void testPorkOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = porkClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
    @Test
    public void testPorkGetDescriptionMethod() {
        String porkOutput = pork.getDescription();
        assertEquals(porkOutput.getClass().getName(), "java.lang.String");
        assertEquals(porkOutput, "Adding Tian Xu Pork Meat...");
    }
}
