package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChickenTest {
    private Class<?> chickenClass;
    private Chicken chicken;

    @BeforeEach
    public void setUp() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        chicken = new Chicken();
    }
    @Test
    public void testChickenIsAMeat() throws Exception {
        Collection<Type> interfaces = Arrays.asList(chickenClass.getInterfaces());
        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName().equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }
    @Test
    public void testChickenOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = chickenClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }
    @Test
    public void testChickenGetDescriptionMethod() {
        String chickenOutput = chicken.getDescription();
        assertEquals(chickenOutput.getClass().getName(), "java.lang.String");
        assertEquals(chickenOutput, "Adding Wintervale Chicken Meat...");
    }
}
