#### Perbedaan Lazy Instantiation dan Eager Instantiation

Pada Lazy initialization, object dibuat hanya saat dibutuhkan. Keuntungannya akan terhindar dari pembuatan object yang tidak perlu, sehingga dapat meningkatkan kinerja dan mengurangi kebutuhan memori.
Pada Eager initialization, object dibuat pada eksekusi program. Kerugiannya adalah akan menghabiskan memori. Kelebihannya adalah Ini berguna jika objek tersebut wajib dan dalam semua kasus berfungsi.
Jadi, keduanya sama-sama memiliki peran masing-masing dan digunakan sesuai dengan kebutuhan.
