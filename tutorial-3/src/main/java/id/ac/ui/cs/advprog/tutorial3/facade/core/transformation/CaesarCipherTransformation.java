package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarCipherTransformation {
    private int num;

    public CaesarCipherTransformation() {
        this.num = num;
    }

    private Spell transformation(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int edselector = encode ? 1 : -1;
        int n = text.length();
        char[] res = new char[n];

        for(int i = 0; i < n; i++) {
            char oldChar = text.charAt(i);
            int newIdx = codex.getIndex(oldChar) + (num*edselector);
            newIdx = mod(newIdx,codex.getCharSize());
            res[i] = codex.getChar(newIdx);
        }
        return new Spell(new String(res), codex);
    }
    private int mod(int n1, int n2) {
        int n3 = n1 % n2;
        int res = (n3 < 0) ? (n3+n2) :n3;
        return res;
    }
    public Spell encode(Spell spell){
        return transformation(spell, true);
    }

    public Spell decode(Spell spell){
        return transformation(spell, false);
    }

}
