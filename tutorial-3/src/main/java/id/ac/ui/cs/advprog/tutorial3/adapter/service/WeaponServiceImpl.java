package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.*;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;


    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        for (Spellbook spellbook : spellbookRepository.findAll()){
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }
        for (Bow bow: bowRepository.findAll()) {
            weaponRepository.save(new BowAdapter(bow));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        if (attackType == 0) {
            logRepository.addLog(weapon.getHolderName()+" attacked with "+weapon.getName()+" (normal attack): "+weapon.normalAttack());
        }
        else if(attackType == 1) {
            logRepository.addLog(weapon.getHolderName()+" attacked with "+weapon.getName()+" (charged attack): "+weapon.chargedAttack());
        }

    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
